[[ -f ~/.zsh/aliases.zsh ]] && source ~/.zsh/aliases.zsh
[[ -f ~/.zsh/android-sdk.zsh ]] && source ~/.zsh/android-sdk.zsh
[[ -f ~/.zsh/functions.zsh ]] && source ~/.zsh/functions.zsh
[[ -f ~/.zsh/java.zsh ]] && source ~/.zsh/java.zsh
[[ -f ~/.zsh/maven.zsh ]] && source ~/.zsh/maven.zsh
[[ -f ~/.zsh/nvm.zsh ]] && source ~/.zsh/nvm.zsh
[[ -f ~/.zsh/pnpm.zsh ]] && source ~/.zsh/pnpm.zsh
[[ -f ~/.zsh/sdkman.zsh ]] && source ~/.zsh/sdkman.zsh
[[ -f ~/.zsh/starship.zsh ]] && source ~/.zsh/starship.zsh
[[ -f ~/.zsh/yarn.zsh ]] && source ~/.zsh/yarn.zsh

# Load Starship
eval "$(starship init zsh)"
# Giuseppe's "Dotfiles"

Hi, there! 👋

I’m Giuseppe, a 34-year-old developer from Switzerland.

This Repository **Dotfiles** contain my personal config files. Here you'll find configs, customizations, themes, and whatever I need to personalize my Linux and mac OS experience.

## Terminal and Application Icons with Nerd-Fonts

To display icons in terminal or applications Fonts, I'm using [Nerd-Fonts](https://www.nerdfonts.com). I'm currently using the **Caskaydia Cove Nerd Font** in terminal applications.

## Windows/PowerShell/WSL2

The main branch rappresenets only my macOS configuration. Since I'm using the same config (or almost the same) on Windows as well, I created a branch called wsl2. On that branch you can find my Windows Configuration including PowerShell and WSL.

I try my best to keep them in sync but of course it's not always going to be exactly the same.

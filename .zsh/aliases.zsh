# Alias
# ---
#
alias h="helm"

# mac OS shortcuts
alias code="open -a 'Visual Studio Code'"

# ALIAS COMMANDS
alias ls="exa --icons --group-directories-first"
alias ll="exa --icons --group-directories-first -l"
alias la="exa --icons --group-directories-first -la"
alias grep='grep --color'
alias rnm="find . -name "node_modules" -type d -prune -exec rm -rf '{}' +"
alias s="source .zshrc"

# ALIAS PROJECTS
alias cws="cd /Users/aqg/workspaces/"

alias csui="code /Users/aqg/workspaces/suisa/suisa.code-workspace"
alias canet="code /Users/aqg/workspaces/my-stuff/aquinonet"
alias colio="code /Users/aqg/workspaces/my-stuff/aquino-web-next"
alias cdot="code /Users/aqg/workspaces/my-stuff/dotfiles"